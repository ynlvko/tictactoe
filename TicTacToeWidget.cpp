#include <QGridLayout>
#include <QPushButton>
#include <QSignalMapper>

#include "TicTacToeWidget.h"

TicTacToeWidget::TicTacToeWidget(QWidget *parent)
    : QWidget(parent)
{
    setupBoard();
}

TicTacToeWidget::~TicTacToeWidget()
{

}

void TicTacToeWidget::setupBoard()
{
    QGridLayout *gridLayout = new QGridLayout;
    QSignalMapper *mapper = new QSignalMapper(this);

    for (int row = 0; row < 3; ++row) {
        for (int column = 0; column < 3; ++column) {
            QPushButton *button = new QPushButton;
            button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
            button->setText(" ");
            gridLayout->addWidget(button, row, column);
            board.append(button);
            mapper->setMapping(button, board.count() - 1);
            connect(button, SIGNAL(clicked(bool)),
                    mapper, SLOT(map()));
        }
    }

    connect(mapper, SIGNAL(mapped(int)),
            this, SLOT(handleButtonClick(int)));

    setLayout(gridLayout);
}

TicTacToeWidget::Player TicTacToeWidget::checkWinCondition(int row, int column)
{
    return Invalid;
}

void TicTacToeWidget::initNewGame() {
    for (int i = 0; i < 9; ++i) {
        board.at(i)->setText(" ");
    }
}

TicTacToeWidget::Player TicTacToeWidget::currentPlayer() const
{
    return m_currentPlayer;
}

void TicTacToeWidget::setCurrentPlayer(TicTacToeWidget::Player p)
{
    if (m_currentPlayer == p) return;
    m_currentPlayer = p;
    emit currentPlayerChanged(p);
}

void TicTacToeWidget::handleButtonClick(int index)
{
    // out of bounds check
    if (index < 0 || index >= board.size()) return;

    QPushButton *button = board.at(index);

    // Invalid move
    if (button->text() != " ") return;

    button->setText(currentPlayer() == Player1 ? "X" : "O");
    Player winner = checkWinCondition(index / 3, index % 3);
    if (winner == Invalid) {
        setCurrentPlayer(currentPlayer() == Player1 ? Player2 : Player1);
        return;
    } else {
        emit gameOver(winner);
    }
}
