#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <TicTacToeWidget.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void startNewGame();
    void handleGameOver(TicTacToeWidget::Player winner);

private:
    Ui::MainWindow *ui;

private slots:
    void updateNameLabels();
};

#endif // MAINWINDOW_H
