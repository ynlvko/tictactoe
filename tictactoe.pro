#-------------------------------------------------
#
# Project created by QtCreator 2017-04-03T12:03:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tictactoe
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        TicTacToeWidget.cpp \
    ../../../Workspace/qt/tictactoe/ConfigurationDialog.cpp \
    ../../../Workspace/qt/tictactoe/MainWindow.cpp

HEADERS  += TicTacToeWidget.h \
    ../../../Workspace/qt/tictactoe/ConfigurationDialog.h \
    ../../../Workspace/qt/tictactoe/MainWindow.h

FORMS += \
    ../../../Workspace/qt/tictactoe/ConfigurationDialog.ui \
    ../../../Workspace/qt/tictactoe/MainWindow.ui

RESOURCES += \
    resources.qrc
