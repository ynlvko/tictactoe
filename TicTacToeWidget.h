#ifndef TICTACTOEWIDGET_H
#define TICTACTOEWIDGET_H

#include <QWidget>

class QPushButton;

class TicTacToeWidget : public QWidget
{
    Q_OBJECT
    Q_ENUMS(Player)
    Q_PROPERTY(Player currentPlayer
               READ currentPlayer
               WRITE setCurrentPlayer
               NOTIFY currentPlayerChanged)

public:
    TicTacToeWidget(QWidget *parent = 0);
    ~TicTacToeWidget();

    enum Player {
        Invalid, Player1, Player2, Draw
    };

    void initNewGame();
    Player currentPlayer() const;
    void setCurrentPlayer(Player p);

signals:
    void currentPlayerChanged(Player);
    void gameOver(TicTacToeWidget::Player);

public slots:
    void handleButtonClick(int);

private:
    QList<QPushButton*> board;
    Player m_currentPlayer;

    void setupBoard();
    Player checkWinCondition(int row, int column);
};

#endif // TICTACTOEWIDGET_H
